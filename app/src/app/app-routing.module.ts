import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePageComponent } from './home-page/home-page.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { BlogComponent } from './blog/blog.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { JoinUsComponent } from './join-us/join-us.component';
import { MyAccountComponent } from './my-account/my-account.component';
import { TrainingsComponent } from './trainings/trainings.component';
import { TrainerRegistrationComponent } from './join-us/trainer-registration/trainer-registration.component';
import { StudentRegistrationComponent } from './join-us/student-registration/student-registration.component';
import { AuthGuardService } from './auth-guard.service';
import { AccountHomeComponent } from './home-page/account-home/account-home.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomePageComponent },
  { path: 'about-us', component: AboutUsComponent },
  { path: 'blog', component: BlogComponent },
  { path: 'sign-in', component: SignInComponent },
  { path: 'trainer-join', component: TrainerRegistrationComponent },
  { path: 'student-join', component: StudentRegistrationComponent },
  { path: 'join-us', component: JoinUsComponent },
  { path: 'my-account', component: MyAccountComponent, canActivate: [AuthGuardService] },
  { path: 'trainings', component: TrainingsComponent, canActivate: [AuthGuardService] },
  { path: 'home-account', component: AccountHomeComponent, canActivate: [AuthGuardService] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
