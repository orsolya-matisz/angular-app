import { Component, EventEmitter, Output } from '@angular/core';
import { UserService } from '../user-serivce/user.service';
import { UserRole } from '../user-role.enum';
import { Router } from '@angular/router';

@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.scss'],
})
export class SideNavComponent {
  @Output() closeSideMenu = new EventEmitter<void>();
  userRole?: UserRole.Student | UserRole.Trainer;

  constructor(public userService: UserService, private router: Router) {
    this.userRole = this.userService.getCurrentUserRole();
  }

  logout() {
    this.router.navigate(['/home']);
    this.userService.logout();
  }

  close() {
    this.closeSideMenu.emit();
  }
}
