import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-student-registration',
  templateUrl: './student-registration.component.html',
  styleUrls: ['./student-registration.component.scss'],
})
export class StudentRegistrationComponent {
  loading = false;
  submitted = false;
  studentFormData: any = {};

  submitStudentForm(studentForm: NgForm) {
    this.loading = true;
    this.studentFormData = studentForm.value;
    setTimeout(() => {
      this.submitted = true;
      this.loading = false;
    }, 2000);
  }
}
