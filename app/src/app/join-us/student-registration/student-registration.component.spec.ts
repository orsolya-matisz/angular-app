import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentRegistrationComponent } from './student-registration.component';
import { SharedButtonComponent } from '../../shared/shared-button/shared-button.component';
import { HeaderComponent } from '../../header/header.component';
import { MatIconModule } from '@angular/material/icon';
import { FormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';

describe('StudentRegistrationComponent', () => {
  let component: StudentRegistrationComponent;
  let fixture: ComponentFixture<StudentRegistrationComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MatFormFieldModule, MatIconModule, FormsModule],
      declarations: [
        StudentRegistrationComponent,
        SharedButtonComponent,
        HeaderComponent,
      ],
    });
    fixture = TestBed.createComponent(StudentRegistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
