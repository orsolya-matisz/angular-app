import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TrainerRegistrationComponent } from './trainer-registration.component';
import { SharedButtonComponent } from '../../shared/shared-button/shared-button.component';
import { HeaderComponent } from '../../header/header.component';
import { MatIconModule } from '@angular/material/icon';
import { FormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
describe('TrainerRegistrationComponent', () => {
  let component: TrainerRegistrationComponent;
  let fixture: ComponentFixture<TrainerRegistrationComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MatFormFieldModule, MatIconModule, FormsModule],
      declarations: [
        TrainerRegistrationComponent,
        SharedButtonComponent,
        HeaderComponent,
      ],
    });
    fixture = TestBed.createComponent(TrainerRegistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
