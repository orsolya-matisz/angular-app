import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-trainer-registration',
  templateUrl: './trainer-registration.component.html',
  styleUrls: ['./trainer-registration.component.scss'],
})
export class TrainerRegistrationComponent {
  loading = false;
  submitted = false;
  trainerFormData: any = {};

  submitTrainerForm(trainerForm: NgForm) {
    this.loading = true;
    this.trainerFormData = trainerForm.value;
    setTimeout(() => {
      this.submitted = true;
      this.loading = false;
    }, 2000);
  }
}
