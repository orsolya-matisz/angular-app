import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomePageComponent } from './home-page/home-page.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { SharedButtonComponent } from './shared/shared-button/shared-button.component';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatTableModule } from '@angular/material/table';
import { FormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AboutUsComponent } from './about-us/about-us.component';
import { BlogComponent } from './blog/blog.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { JoinUsComponent } from './join-us/join-us.component';
import { MyAccountComponent } from './my-account/my-account.component';
import { TrainingsComponent } from './trainings/trainings.component';
import { TrainerRegistrationComponent } from './join-us/trainer-registration/trainer-registration.component';
import { StudentRegistrationComponent } from './join-us/student-registration/student-registration.component';
import { NavigationMenuComponent } from './navigation-menu/navigation-menu.component';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatButtonModule } from '@angular/material/button';
import { SideNavComponent } from './side-nav/side-nav.component';
import { AccountHomeComponent } from './home-page/account-home/account-home.component';

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    HeaderComponent,
    FooterComponent,
    SharedButtonComponent,
    AboutUsComponent,
    BlogComponent,
    SignInComponent,
    JoinUsComponent,
    MyAccountComponent,
    TrainingsComponent,
    TrainerRegistrationComponent,
    StudentRegistrationComponent,
    NavigationMenuComponent,
    SideNavComponent,
    AccountHomeComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatIconModule,
    MatInputModule,
    FormsModule,
    MatSelectModule,
    BrowserAnimationsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatTableModule,
    MatSidenavModule,
    MatButtonModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
