import { Component } from '@angular/core';
import { UserRole } from 'src/app/user-role.enum';
import { UserService } from 'src/app/user-serivce/user.service';

@Component({
  selector: 'app-account-home',
  templateUrl: './account-home.component.html',
  styleUrls: ['./account-home.component.scss']
})
export class AccountHomeComponent {
  userRole?: UserRole.Student | UserRole.Trainer;
  userData: any[] = [];

  constructor(private userService: UserService) {
    this.userRole = this.userService.getCurrentUserRole();
    if (this.userRole === UserRole.Student) {
      this.userData = this.userService.getUserByRole(UserRole.Student);
    } else if (this.userRole === UserRole.Trainer) {
      this.userData = this.userService.getUserByRole(UserRole.Trainer);
    }
  }
}
