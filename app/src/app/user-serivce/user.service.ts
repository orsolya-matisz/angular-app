// user.service.ts
import { Injectable } from '@angular/core';
import { UserRole } from '../user-role.enum';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private readonly userKey = 'currentUser';

  authenticateUser(username: string, password: string) {
    const users = this.getUsers();
    const authenticatedUser = users.find(
      (user) => user.username === username && user.password === password
    );

    if (authenticatedUser) {
      this.setCurrentUser(authenticatedUser);
      return true;
    }

    return false;
  }

  getCurrentUser() {
    const storedUser = localStorage.getItem(this.userKey);
    return storedUser ? JSON.parse(storedUser) : null;
  }

  getCurrentUserRole() {
    const currentUser = this.getCurrentUser();
    return currentUser ? currentUser.role : null;
  }

  isLoggedIn(): boolean {
    return !!this.getCurrentUser();
  }

  logout() {
    localStorage.removeItem(this.userKey);
  }

  getUserByRole(role: UserRole) {
    const users = this.getUsers();
    return users.filter((user) => user.role === role);
  }

  getUserUsername(): string | null {
    const currentUser = this.getCurrentUser();
    return currentUser ? currentUser.username : null;
  }

  getUserEmail(): string | null {
    const currentUser = this.getCurrentUser();
    return currentUser ? currentUser.email : null;
  }

  private setCurrentUser(user: any) {
    localStorage.setItem(this.userKey, JSON.stringify(user));
  }

  private getUsers() {
    return [
      {
        firstName: 'John',
        lastName: 'Black',
        specialization: 'Java',
        address: '456 Lake shore Drive, Chicago, IL 60611US',
        email: 'Jiohn_12@gmail.com',
        username: 'Jihn_12',
        password: 'trainer123',
        role: UserRole.Trainer,
      },
      {
        firstName: 'Marta',
        lastName: 'Black',
        dateOfBirth: '01.01.2001',
        address: '123 Main Street Boston, MA 02108US',
        email: 'marta_12334&gmail.com',
        username: 'Marta_st',
        password: 'student123',
        role: UserRole.Student,
      },
    ];
  }
}
