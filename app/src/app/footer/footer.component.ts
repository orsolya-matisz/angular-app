import { animate, style, transition, trigger } from '@angular/animations';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
  animations: [
    trigger('transitionMessages', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('300ms', style({ opacity: 1 })),
      ]),
    ]),
  ],
})
export class FooterComponent implements OnInit {
  selectedOption!: string; // Stores the selected option
  options: string[] = ['English', 'Hungarian']; // Define your options here

  ngOnInit() {
    this.selectedOption = 'English';
  }
}
