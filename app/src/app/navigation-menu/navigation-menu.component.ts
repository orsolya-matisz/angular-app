import { Component } from '@angular/core';
import { UserService } from '../user-serivce/user.service';

@Component({
  selector: 'app-navigation-menu',
  templateUrl: './navigation-menu.component.html',
  styleUrls: ['./navigation-menu.component.scss'],
})
export class NavigationMenuComponent {
  constructor(public userService: UserService) {}
}
