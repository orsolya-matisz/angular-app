import { Component } from '@angular/core';
import { UserRole } from '../user-role.enum';
import { UserService } from '../user-serivce/user.service';

@Component({
  selector: 'app-my-account',
  templateUrl: './my-account.component.html',
  styleUrls: ['./my-account.component.scss'],
})
export class MyAccountComponent {
  userRole?: UserRole.Student | UserRole.Trainer;
  userData: any[] = [];

  constructor(private userService: UserService) {
    this.userRole = this.userService.getCurrentUserRole();
    if (this.userRole === UserRole.Student) {
      this.userData = this.userService.getUserByRole(UserRole.Student);
    } else if (this.userRole === UserRole.Trainer) {
      this.userData = this.userService.getUserByRole(UserRole.Trainer);
    }
  }
}
