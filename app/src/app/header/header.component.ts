import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { PageInfoService } from '../page-info.service';
import { UserService } from '../user-serivce/user.service';
import { UserRole } from '../user-role.enum';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  isSideMenuOpen = false;
  isClosed = false;
  isDropdownOpen = false;
  userRole?: UserRole.Student | UserRole.Trainer;

  constructor(
    public pageInfoService: PageInfoService,
    public userService: UserService,
    private router: Router
  ) {
    this.userRole = this.userService.getCurrentUserRole();
  }

  ngOnInit(): void {}

  toggleSideMenu() {
    this.isSideMenuOpen = !this.isSideMenuOpen;
    console.log('open');
  }

  closeSideMenu() {
    this.isClosed = true;
  }

  openDropdown() {
    this.isDropdownOpen = true;
  }

  closeDropdown() {
    this.isDropdownOpen = false;
  }

  logout() {
    this.router.navigate(['/home']);
    this.userService.logout();
  }
}
