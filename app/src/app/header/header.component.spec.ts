import {
  ComponentFixture,
  TestBed,
  tick,
  fakeAsync,
} from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { MatIconModule } from '@angular/material/icon';
import { FormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { HeaderComponent } from './header.component';
import { SharedButtonComponent } from '../shared/shared-button/shared-button.component';
import { SideNavComponent } from '../side-nav/side-nav.component';
import { NavigationMenuComponent } from '../navigation-menu/navigation-menu.component';
import { PageInfoService } from '../page-info.service';
import { UserService } from '../user-serivce/user.service';
import { Router } from '@angular/router';
import { of } from 'rxjs';
import { UserRole } from '../user-role.enum';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;
  let userService: jasmine.SpyObj<UserService>;
  let pageInfoService: jasmine.SpyObj<PageInfoService>;
  let router: Router;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        MatFormFieldModule,
        MatIconModule,
        FormsModule,
        RouterTestingModule,
      ],
      declarations: [
        HeaderComponent,
        SharedButtonComponent,
        SideNavComponent,
        NavigationMenuComponent,
      ],
      providers: [
        {
          provide: PageInfoService,
          useValue: jasmine.createSpyObj('PageInfoService', ['getCurrentPage']),
        },
        {
          provide: UserService,
          useValue: jasmine.createSpyObj('UserService', [
            'logout',
            'getCurrentUserRole',
          ]),
        },
      ],
    });

    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    userService = TestBed.inject(UserService) as jasmine.SpyObj<UserService>;
    pageInfoService = TestBed.inject(
      PageInfoService
    ) as jasmine.SpyObj<PageInfoService>;
    router = TestBed.inject(Router);
  });

  it('must log out and navigate to home when logout is called', fakeAsync(() => {
    const navigateSpy = spyOn(router, 'navigate');
    userService.logout.and.stub();
    userService.getCurrentUserRole.and.returnValue(UserRole.Student); // Assuming the user is a student

    component.logout();

    tick();

    expect(userService.logout).toHaveBeenCalled();
    expect(navigateSpy).toHaveBeenCalledWith(['/home']); // Expecting navigation to home page
  }));
});
