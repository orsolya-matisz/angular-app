import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-shared-button',
  templateUrl: './shared-button.component.html',
  styleUrls: ['./shared-button.component.scss'],
})
export class SharedButtonComponent {
  @Input() buttonStyle: 'primary' | 'secondary' = 'primary';
  @Input() buttonSize: 'normal' | 'wide' | 'submit' = 'normal';
}
