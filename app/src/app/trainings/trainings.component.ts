import { Component } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { UserService } from '../user-serivce/user.service';
import { UserRole } from '../user-role.enum';

@Component({
  selector: 'app-trainings',
  templateUrl: './trainings.component.html',
  styleUrls: ['./trainings.component.scss'],
})
export class TrainingsComponent {
  userRole?: UserRole.Student | UserRole.Trainer;
  userData: any[] = [];
  passedTrainings = [
    {
      date: '12.03.2023',
      trainingName: 'JavaScript Course 1',
      type: 'Webinar',
      trainerName: 'Matthew Martinez',
      duration: '15 d',
    },
    {
      date: '12.03.2023',
      trainingName: 'JS Course 2',
      type: 'Webinar',
      trainerName: 'Matthew Martinez',
      duration: '10 d',
    },
    {
      date: '12.03.2023',
      trainingName: 'Java',
      type: 'Webinar',
      trainerName: 'Maria White',
      duration: '2 d',
    },
  ];

  results = [
    {
      date: '12.03.2023',
      trainingName: 'JavaScript Course 1',
      type: 'Webinar',
      studentName: 'Marta Black',
      duration: '15 d',
    },
    {
      date: '12.03.2023',
      trainingName: 'Course 2',
      type: 'Webinar',
      studentName: 'Student 1',
      duration: '10 d',
    },
    {
      date: '12.03.2023',
      trainingName: 'Course 3',
      type: 'Webinar',
      studentName: 'Student 2',
      duration: '2 d',
    },
    {
      date: '12.03.2023',
      trainingName: 'Course 4',
      type: '',
      studentName: 'Student 3',
      duration: '',
    },
    {
      date: '12.03.2023',
      trainingName: 'Course 5',
      type: '',
      studentName: '',
      duration: '',
    },
  ];

  constructor(private userService: UserService) {
    this.userRole = this.userService.getCurrentUserRole();
    if (this.userRole === UserRole.Student) {
      this.userData = this.userService.getUserByRole(UserRole.Student);
    } else if (this.userRole === UserRole.Trainer) {
      this.userData = this.userService.getUserByRole(UserRole.Trainer);
    }
  }
}
