import { Component } from '@angular/core';
import { UserService } from '../user-serivce/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss'],
})
export class SignInComponent {
  signInFormModel = {
    username: '',
    password: '',
  };
  showPassword = false;

  constructor(private userService: UserService, private router: Router) {}

  login(form: any) {
    const { username, password } = form.value;
    const isAuthenticated = this.userService.authenticateUser(
      username,
      password
    );

    if (!isAuthenticated) {
      console.error('Invalid credentials');
    }
  }

  signIn() {
    const { username, password } = this.signInFormModel;

    const isAuthenticated = this.userService.authenticateUser(
      username,
      password
    );

    if (isAuthenticated) {
      const userRole = this.userService.getCurrentUserRole();

      console.log(`Logged in as ${userRole}`);

      if (userRole === 'student') {
        this.router.navigate(['/home-account']);
      } else if (userRole === 'trainer') {
        this.router.navigate(['/home-account']);
      }
    } else {
      console.error('Invalid credentials');
    }
  }

  togglePasswordVisibility() {
    this.showPassword = !this.showPassword;
  }
}
