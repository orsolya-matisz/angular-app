import {
  ComponentFixture,
  TestBed,
  tick,
  fakeAsync,
} from '@angular/core/testing';
import { SignInComponent } from './sign-in.component';
import { UserService } from '../user-serivce/user.service';
import { Router } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { By } from '@angular/platform-browser';
import { SharedButtonComponent } from '../shared/shared-button/shared-button.component';


describe('SignInComponent', () => {
  let component: SignInComponent;
  let fixture: ComponentFixture<SignInComponent>;
  let userService: UserService;
  let router: Router;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MatIconModule, FormsModule, MatFormFieldModule],
      declarations: [SignInComponent, SharedButtonComponent],
      providers: [UserService, Router],
    });
    fixture = TestBed.createComponent(SignInComponent);
    component = fixture.componentInstance;
    userService = TestBed.inject(UserService);
    router = TestBed.inject(Router);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('must show error message on unsuccessful login', fakeAsync(() => {
    spyOn(userService, 'authenticateUser').and.returnValue(false);
    spyOn(console, 'error'); // Suppress console.error output for cleaner test logs

    // Trigger the login form submission
    component.login({ value: { username: 'test', password: 'password' } });
    tick();

    // Check if console.error was called with the correct message
    expect(console.error).toHaveBeenCalledWith('Invalid credentials');
  }));
});
